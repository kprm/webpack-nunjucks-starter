const path = require('path');
const glob = require('glob');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const baseConfig = require('./base.cfg');
const stylelintCfg = require('../.stylelintrc');


const pages = glob.sync('*.html', {
  cwd: path.join(__dirname, '../src/'),
  root: '/'
}).map(page => new HtmlWebpackPlugin({
  filename: `${page}`,
  template: path.join(__dirname, `../src/${page}`),
  inject: true
}));

module.exports = merge(baseConfig, {
  mode: 'development',
  devServer: {
    stats: 'errors-only',
    hot: true,
    port: 3000,
    quiet: true,
    overlay: {
      warnings: true,
      errors: true
    }
  },
  devtool: 'eval',
  
  plugins: [
    new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
    }),
    new FriendlyErrorsWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
      disable: false,
      allChunks: true
    }),
    ...pages,
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  module: {
    rules: [
      // {
      //   test: /\.js$/,
      //   exclude: /node_modules/,
      //   use: {
      //     loader: 'eslint-loader',
      //     options: {
      //       fix: true
      //     }
      //   }
      // },
      {
        test: /\.(png|jpe?g|gif|ico|webp)$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'file-loader',
            options: {
              context: path.resolve(__dirname, '../src/'),
              name: '[path][name].[hash:7].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(css|scss|sass)$/,
        use: [
          'css-hot-loader',
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../'
            }
          },
          'css-loader',
          // {
          //   loader: 'postcss-loader',
          //   options: {
          //     // sourceMap: true,
          //     plugins: (loader) => [
          //       require('stylelint')(stylelintCfg)
          //       // require('autoprefixer')({
          //       //   'browsers': ['last 2 versions', 'safari >= 7', 'ie >= 9', 'ios >= 6']
          //       // })
          //     ]
          //   }
          // },
          // 'resolve-url-loader',
          'sass-loader' // ?sourcemap when resolve-url-loader enabled
        ]
      }
    ]
  }
})
